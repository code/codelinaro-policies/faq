The primary purpose of the CodeLinaro community is to collaborate on software
projects. We want people to work better together.

There are a variety of actions that we may take when a user reports
inappropriate behavior or content. Actions we may take in response to an abuse
report include but are not limited to:

  * Content Removal

  * Content Blocking

  * Account Suspension

  * Account Termination

If any of these actions have occurred and you believe this is in error, you
may appeal for reinstatement. In some cases, there may be a basis to reverse
an action. To appeal, please _[contact
us](https://www.codelinaro.org/contact/)_ and explain how you have addressed
the violation and will agree to abide by the CodeLinaro Acceptable Use
Policies.

Please note, CodeLinaro retains complete discretion under the Terms of Service
to remove any content or terminate any accounts for the activity that violates
the _[CodeLinaro Acceptable Use
Policies](https://docs.google.com/document/u/1/d/13gXernFgsdtA8k188IiaUGN7xXg2u3ni1BBJFJIZiVI/edit)_.

