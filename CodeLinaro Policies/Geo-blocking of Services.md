CodeLinaro's services, Artifactory, and GitLab do not support geo-blocking at
the project repository level.  CodeLinaro can provide blocking by an IP
address or a range of IP addresses of a geographical region. Please note -
Blocking by IP address will prevent access to the entire service.

To enact blocking by IP address, a customer must send a list of IPs to block
via Service Support ticket.  Please use **All other Requests** -> **Service
Request** and provide the list of IP addresses.

CodeLinaro will send the list of IPs to our service vendors accordingly to
place on a restricted access list, denying any attempts to access these
CodeLinaro Services from the requested list of IPs.

  

