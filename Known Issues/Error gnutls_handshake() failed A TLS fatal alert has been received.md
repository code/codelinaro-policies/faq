Upon cloning, if the following error is observed on Linux only…

`git clone https://git.codelinaro.org/{path/to/project/repo}.git`  
`Cloning into 'project-location'...`  
`error: gnutls_handshake() failed: A TLS fatal alert has been received`  
`while accessing
https://git.codelinaro.org/{path/to/project/repo}.git/info/refs?service=git-
upload-pack`  
`fatal: HTTP request failed`

 **Linux/Ubuntu**  
Git uses gnuTLS by default on Ubuntu. If you are using a version of Ubuntu
that is older than 14.10, You must **upgrade the Linux Git client version to
1.8.1.2 or higher** and either configure Git to **use openSSL instead of
gnuTLS** or upgrade Ubuntu to 14.10 or higher, which uses a version of gnuTLS
that is compatible.

`$ gnutls-cli -v -V`  
`gnutls-cli 3.3.8`  

ubuntu 15.04 : 3.3.8 - Supported  
ubuntu 14.10 : 3.2.15 - Supported  
ubuntu 14.04 : 2.12.23 - Unsupported  
ubuntu 12.04: 2.12.14 - Unsupported  

 **Windows**  
This error is not known to occur on Windows Git clients. However, Windows
users should continue to upgrade as new releases of Git become available.
Also, most importantly, be sure to check the state of your host machine's
Windows Firewall. We have had customers report TCP releases\resets when they
did not realize that their Windows Firewall was enabled. Disabling the Windows
Firewall is a first step in troubleshooting these issues.  

Upon cloning, if the following error is observed on Linux only…

`git clone https://git.codelinaro.org/{path/to/project/repo}.git`  
`Cloning into 'project-location'...`  
`fatal: unable to access : GnuTLS recv error (-24): Decryption has failed.`

This is a network connectivity issue. Please check your network connection and
try again. It is not a CodeLinaro issue. Also, you should use openSSL instead
of gnuTLS

