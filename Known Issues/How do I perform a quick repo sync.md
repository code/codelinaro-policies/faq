The [repo](https://git.codelinaro.org/clo/tools/repo) tool supports being able
to quickly sync a manifest file in a couple of ways - Shallow Sync(1 depth)
and/or Network only Sync.

## Shallow Sync

Performing a shallow sync means the repo tool will only fetch and check out
every repository's latest commit or snapshot in the manifest file. It utilizes
the `--depth=1` [git argument](https://git-scm.com/docs/git-
fetch#Documentation/git-fetch.txt---depthltdepthgt) when performing the fetch
commands during the `repo sync` process.

To perform a shallow sync, you must:

  * Initialize the local folder: `repo init -u <<URL to manifest repository>>.git -b release -m <<manifest filename>>.xml --depth=1`

  * Run the sync process: `repo sync -j8 --no-clone-bundle`

Additional notes:

  * `-j8` means the number of parallel jobs to run. 

  * `--no-clone-bundle` this parameter prevents the repo tool from needlessly requesting `clone.bundle` files for each repository as CodeLinaro does not host these filetypes. 

## Network Only Sync

Performing a network-only sync might be beneficial if used to determine
performance without checking out or decompressing the delta objects that
typically come from git fetch. To perform this, do the same steps as the
shallow sync above. However, for the sync step, you need to specify the `-n`
argument:

  * `repo sync -j8 -n --no-clone-bundle`

This will just fetch the objects but leave them compressed. IF YOU NEED THE
SOURCE, DO NOT USE THIS METHOD.

