Due to varying network environments, users might experience randomly occurring
timeouts or broken pipe errors.

Many of such users have had great success when adding the following host
configuration to the ~/.ssh/config file.

……………

Host git.codelinaro.org<br>
&nbsp;&nbsp;&nbsp;&nbsp;ServerAliveInterval 60<br>
&nbsp;&nbsp;&nbsp;&nbsp;ServerAliveCountMax 5<br>
&nbsp;&nbsp;&nbsp;&nbsp;TCPKeepAlive yes<br>
&nbsp;&nbsp;&nbsp;&nbsp;IPQoS=throughput

