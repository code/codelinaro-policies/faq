Depending on your server and network configurations, you might experience the
following error when fetching or cloning a large repository from Git
CodeLinaro:

 _Receiving objects: 91% (6768/7420), 6.66 GiB | 6.33 MiB/s_  
 _Receiving objects: 91% (6768/7420), 6.66 GiB | 6.37 MiB/s_  
 _Receiving objects: 91% (6768/7420), 6.67 GiB | 7.13 MiB/s_  
 _Receiving objects: 91% (6769/7420), 6.67 GiB | 6.61 MiB/s_  
 _Receiving objects: 91% (6769/7420), 6.68 GiB | 5.54 MiB/s_

 _Receiving objects: 91% (6769/7420), 6.68 GiB | 5.54 MiB/s_  
 _error: RPC failed; curl 56 GnuTLS recv error (-54): Error in the pull
function._  
 _fatal: The remote end hung up unexpectedly_  
 _fatal: early EOF_  
 _fatal: index-pack failed_  
 _error: Cannot fetch platform/external/chromium-webview from
https://git.codelinaro.org/clo/la/platform/external/chromium-webview_

## Solution 1: Update Libraries

Make sure you have the latest GnuTLS and OpenSSL libraries installed on your
machine.

If using Linux/Ubuntu, try running: `apt-get install gnutls-bin openssl`

Now, try to clone or fetch again.

## Solution 2: Increase Cache

The local cache is not enough. Try increasing the cache. The unit is b
(bytes), 1048576000 = 1 GB

Run the following to update your local .gitconfig file:

  * `git config --global http.postBuffer 1048576000`

  * `git config --global http.maxRequestBuffer 1048576000`

Now, try to clone or fetch again.

## Solution 3: Network Speed Factors

The download speed might be limited depending on the machine and network
settings.

Run the following to update your local .gitconfig file:

  * `git config --global http.lowSpeedLimit 0`

  * `git config --global http.lowSpeedTime 999999`

Now, try to clone or fetch again.

