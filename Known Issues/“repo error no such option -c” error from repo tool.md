If you are having "repo: error: no such option: -c" error message when running
repo command, please update your repo tool and try again.

Some old repo versions do not support -c option and only supports --current-
branch option.

Error example:  
$ repo init --depth=1 -c -u
<https://git.codelinaro.org/clo/la/la/system/manifest.git> -b release -m
AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.091.04.xml -repo-
url=<https://git.codelinaro.org/clo/tools/repo.git> \--repo-branch=aosp-
new/stable  

Usage: repo init -u url [options]

repo: error: no such option: -c

Here are the steps to update your repo tool:  
$ mkdir -p ~/.bin  
$ PATH="${HOME}/.bin:${PATH}"  
$ curl <https://storage.googleapis.com/git-repo-downloads/repo> > ~/.bin/repo  
$ chmod a+rx ~/.bin/repoReference: <https://gerrit.googlesource.com/git-repo>

