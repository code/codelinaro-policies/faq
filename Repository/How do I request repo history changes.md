To commit history to be altered, the CodeLinaro Support Team will need to
rebase (move or combine a sequence of commits to a new base commit) the
repository within Git. Your administrator will need to raise a [support
request](https://codelinaro.org/inner-support/service_request/) to request for
this operation and provide the following details:

  * Project 

  * Repository URL 

  * Commits (via SHA keys) needing to be moved or combined 

  * If appending to an existing commit, let us know which SHA key and the message needs to be altered. Otherwise, a new commit will occur. Please let us know the commit message to apply. 

  * List users who need to approve or be made aware of such change. 

