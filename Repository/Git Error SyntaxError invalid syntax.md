**When trying to**   **synchronize code from CodeLinaro using the repo tool,
we are seeing the following error:**  

Downloading Repo source from <https://git.codelinaro.org/clo/tools/repo.git>

remote: Enumerating objects: 480, done.

remote: Counting objects: 100% (480/480), done.

remote: Compressing objects: 100% (477/477), done.

remote: Total 6889 (delta 312), reused 0 (delta 0), pack-reused 6409

Receiving objects: 100% (6889/6889), 4.48 MiB | 0 bytes/s, done.

Resolving deltas: 100% (3974/3974), done.

Traceback (most recent call last):

 _File "/…/QSSI/.repo/repo/main.py", line 42, in <module>_

     _from git_config import RepoConfig_

 _File "/…/QSSI/.repo/repo/git_config.py", line 770_

     _self._Set(f'superproject.{key}', value)_

                                   _^_

 _SyntaxError: invalid syntax_

 **Solution**

The error occurs when you are using an  **unsupported**  Python version for
the given version of the  **repo tool**. Please upgrade to Python 3.6 or
later.

