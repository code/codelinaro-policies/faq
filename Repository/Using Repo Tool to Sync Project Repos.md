Follow the [Install Repo](https://source.android.com/setup/develop#installing-
repo) instructions to install the Repo tool.

  
 **Note** :  
The latest version should be used; versions prior to v2.15 have a bug that  
may return a 400 error when attempting to use a manifest from CodeLinaro

