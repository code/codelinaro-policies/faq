**Issue: I am trying to sync code and I am seeing the following error:**

fatal: Cannot get <https://git.codelinaro.org/clo/tools/repo.git/clone.bundle>  
fatal: HTTP error 400  
fatal: cloning the git-repo repository failed, will remove '.repo/repo'

 **How do I avoid this issue?**

You are running an older version of the tools, please install the latest
version of the repo launcher (per <https://source.android.com/setup/develop>
).

