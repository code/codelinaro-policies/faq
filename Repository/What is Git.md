CodeLinaro leverages the latest community edition of Git to handle management
of all project repositories. Git keeps a historic record of each project's
contributions. Maintaining code is easy and reliable as CodeLinaro makes use
of AWS Cloud Services.

