All CodeLinaro projects that utilize the Wiki service also come with an RSS
feed which is updated in real-time as new release notes or other articles
pertaining to said project are published!

Simply select the project of interest, and click on the RSS icon. A new tab
will open displaying the feed content in XML format. Copy the URL and paste it
into your RSS feed reading client.

You'll then get notified by your feed client of any new content that becomes
available within moments!

