By default, your git email address is configured to match the primary email
address associated with your CodeLinaro Account. You can change this to
another email address if preferred by viewing and updating your [GitLab
Profile Settings](https://git.codelinaro.org/profile). Please be sure to
follow the guidelines set by your organization.

