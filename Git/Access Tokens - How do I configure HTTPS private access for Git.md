A Personal Access Token (PAT) is required for git operations using HTTPS
connections. To create one:

  1. Login to your CodeLinaro Account

  2. Navigate to the [Access Tokens page](https://git.codelinaro.org/-/profile/personal_access_tokens) on <http://git.codelinaro.org> .

  3. Provide a  **Name**  for the token. (e.g. CodeLinaro Projects)

  4. Enter an  **Expires at** : date (optional).

  5. Select appropriate  **Scope(s)**  related to your work (e.g. git clone requires a minimum of **read_repository** ).

  6. Click the  **Create Personal Access Token**  button.

Once you have your personal access token, you can use it with the standard
syntax

 **https:// <GITLAB_USERNAME>:<GITLAB_PERSONAL_ACCESS_TOKEN>@** **< Git URL>**

For more information about Personal Access Tokens, see the related [Help
page](https://git.codelinaro.org/help/user/profile/personal_access_tokens.md)
on CodeLinaro Gitlab.

