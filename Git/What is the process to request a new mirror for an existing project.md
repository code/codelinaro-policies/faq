Your administrator will need to open a support ticket to request this
operation and provide the following details:

  * Project 

  * Source Repository URL 

  * Destination Repository URL 

  * Refspec or branch mapping information (ex. +ref/heads/source-branch/*:ref/heads/destination-branch/*) 

  * Frequency - Once, Daily, or Weekly

[Request mirror for existing project](https://codelinaro.org/inner-
support/create-a-mirror/)

NOTE: depending on the Refspec requested, this could replace the existing
repository, thereby removing any current branch in the destination repository
but not in the source repository.

