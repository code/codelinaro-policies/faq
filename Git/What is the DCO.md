[DCO](https://developercertificate.org/) (Developer Certificate of Origin) was
introduced by the Linux Foundation to enhance the submission process for
software. It's a lightweight mechanism for contributors to certify that they
have the right to submit the code they are contributing to the project, and
acknowledge the contribution is public and can be reused.

DCO can be enabled for any git repository in CodeLinaro via the [Projects -
Git Repo API](https://docs.codelinaro.org/#operation/addProjectGitRepo) or
by [request through CodeLinaro Support](https://www.codelinaro.org/inner-
support/enable-dco-for-repository/) ( _login required_ ).

