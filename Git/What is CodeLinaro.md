CodeLinaro, a service provided by Linaro, hosts open source projects serving
the Arm ecosystem. The projects hosted contain tested open source code
needed to provide upstream enablement for innovative, performance optimized
support for system on a chip (SoC) products and their related ecosystems, and
also serve as a staging area for code that is submitted to upstream projects
such as the Linux kernel and Android. CodeLinaro also mirrors key upstream
projects for use by the community on its git server.

