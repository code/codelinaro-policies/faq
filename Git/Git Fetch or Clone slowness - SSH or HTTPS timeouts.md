CodeLinaro users have reported slowness and timeout errors when cloning from
various large or complex repositories in the CodeLinaro Git service. Automatic
housekeeping has been enabled for all repositories to ensure proper
fetching/cloning performance.

Depending on the needs of a project, users who are " **maintainers** " can
trigger housekeeping outside set intervals for a repository within the GitLab
UI or via GitLab's Projects API housekeeping endpoint:
<https://docs.gitlab.com/ee/api/projects.html#start-the-housekeeping-task-
for-a-project>  

These steps will solve users getting SSH/HTTPS timeouts when fetching/cloning
from Git and GitLab's counting/compression of objects stalling or taking far
too long to complete.

As part of this solution, there could be repositories that need more frequent
housekeeping; these repositories might take much longer than usual to
fetch/clone. Until housekeeping can be triggered, users should expect degraded
performance. A potential workaround of adjusting corporate proxy settings will
allow additional time for these operations to complete.

  

