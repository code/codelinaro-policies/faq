With access to CodeLinaro Git, go to your Git account settings and select "SSH
Keys" from the left sidebar menu, or go here
(<https://git.codelinaro.org/-/profile/keys>).

If you have an SSH key you'd like to use, please add the required information
as instructed. Otherwise, you'll need to create a new SSH key and add it to
your settings.

Please see how to create a new SSH key
(<https://git.codelinaro.org/help/ssh/index#generate-an-ssh-key-pair> ).

If you're still unable to add an SSH key to your Git profile, please raise a
[support request](https://codelinaro.org/inner-support/service_request/) and
provide as much detail as possible for the CodeLinaro Support Team to assist.

