Yes! Users can clone from CodeLinaro Git via SSH. Just make sure you have SSH
installed on your machine. For any public repository, once SSH is installed,
you're ready to clone. For any private repository, please follow [these
steps](/faq/81756180/) to set up your SSH key in your Git profile.

