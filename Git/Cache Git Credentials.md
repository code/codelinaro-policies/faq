To avoid being prompted to provide username and password information, CLO Git
credentials can be stored in the **.netrc** file.

When syncing multiple repositories over an HTTPS connection using the Repo
tool, git will prompt for a username and password every time a repository is
being cloned. This can be avoided by storing the credentials in the git
credential system or .netrc file.

To store CLO credentials in the git credential helper, run the following
command:  
`git config --global credential.helper store`

To store credentials in a .netrc file, add the following lines below in the
.netrc file:  
machine <http://git.codelinaro.org>  
login <CodeLinaro username>  
password <CodeLinaro personal access token>

