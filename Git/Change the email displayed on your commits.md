A commit email is an email address displayed in every Git-related action
carried out through the CodeLinaro GitLab interface.

Any of your own verified email addresses can be used as the commit email. Your
primary email is used by default.

To change your commit email, you must first be logged into CodeLinaro and
access the "Git" service. Then:

  1. In the top-right corner, select your avatar.

  2. Select **Edit profile**.

  3. In the **Commit email** dropdown list, select an email address.

  4. Select **Update profile settings**.

