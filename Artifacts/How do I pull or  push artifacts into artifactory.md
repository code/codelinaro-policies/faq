CI build jobs produce artifacts which are usually used for further testing to
proof the desired behavior. In this case, artifacts must be uploaded to an
external place called _jfrog_ _artifactory_. Although there are several ways
to upload artifacts, we recommend using the `jfrog` tool, a command line tool
that can push (or fetch) produced artifacts. Before you can use the tool, the
corresponding CLO project must have the [artifacts service
enable](https://www.codelinaro.org/faq/253231141/) and you have the
artifactory [API KEY](https://www.codelinaro.org/faq/81756220/). Below is a
pipeline example with two jobs, uploading and downloading artifacts

 **default:**

 **image: debian:bullseye-slim**

 **variables:**

 **PUBLISH_SERVER:**  
 **value: '**[
**https://artifacts.codelinaro.org/artifactory'**](https://artifacts.codelinaro.org/artifactory')  
 **description: 'CodeLinaro publish server'**

 **PUB_SRC:**  
 **value: 'linaro-345-test/$CI_PROJECT_NAME/${CI_PIPELINE_ID}'**  
 **description: ''**

 **PUB_DEST:**  
 **value: 'linaro-345-test/$CI_PROJECT_NAME/${CI_PIPELINE_ID}'**  
 **description: 'CodeLinaro publish server'**

 **JGROG_DRY_RUN:**  
 **value: 'false'**  
 **description: 'Boolean defining upload dry-run'**

 **before_script:**

 **\- apt-get update**  
 **\- DEBIAN_FRONTEND=noninteractive apt-get install -y curl**  
 **\- cd /usr/local/bin; curl -fL**[
**https://getcli.jfrog.io**](https://getcli.jfrog.io) **| sh**

 **stages:**

 **-upload**

 **-download**

 **upload:**

 **stage: upload**

 **variables:**

 **UPLOAD_DIR: 'upload_dir'**

 **script: |**

 **set -ex**

 **# create upload directory containing files to be uploaded**

 **mkdir -p $UPLOAD_DIR touch $UPLOAD_DIR/foo $UPLOAD_DIR/boo**

 **#upload**

 **jfrog rt u \**

 **\--flat=true --include-dirs=true --symlinks=true \**

 **\--dry-run=${JGROG_DRY_RUN} \**

 **\--apikey ${ARTIFACTS_API_KEY} \**

 **\--url ${PUBLISH_SERVER}/ \**

 **" ${UPLOAD_DIR}/*" ${PUB_DEST}/**

 **download:**

 **stage: download**

 **image: debian:bullseye-slim**

 **variables:**

 **DOWNLOAD_DIR: 'download_dir'**

 **script: |**

 **set -ex**

 **mkdir -p $DOWNLOAD_DIR**

 **# download**

 **jfrog rt dl \**

 **\--flat=true \**

 **\--dry-run=${JGROG_DRY_RUN} \**

 **\--apikey ${ARTIFACTS_API_KEY} \**

 **\--url ${PUBLISH_SERVER}/ \**

 **${PUB_SRC}/ ${DOWNLOAD_DIR}/**

