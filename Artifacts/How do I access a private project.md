You can only access private projects as determined by your administrator. If
you would like access to a specific private project, please contact your
administrator. Another option would be to raise a [support
request](https://www.codelinaro.org/inner-support/add-users-to-project-
role/), and if your administrator approves, our CodeLinaro Support Team will
address it accordingly.

If you need access to the Git trees or Artifacts for said project, you'll
need to first make sure you are entitled to Git and Artifacts services.

Discuss with your administrator which level of access you need based on the
following:

  *  **Reporter** \- This role is assigned to users needing "read-only" access to the project's GitLab and Artifactory repositories so long as the project is not inactive.

  *  **Developer** \- This role is given to users who will be contributing to this project's development. They will be able to clone any repository from GitLab and submit pull requests if enabled. They will also have "read-only" access to the project's Artifactory repository as uploads are reserved for Maintainers.

  *  **Maintainer** \- This role is given to users who may not only contribute to this project's development, but also approve pull requests and be responsible for releases. They will have full control over this project's GitLab and Artifactory repositories.

If you're an editor and need to be able to publish/modify the project's wiki
information, you'll need the "editor" access level granted.

  *  **Editor** \- This role is assigned to users who need the ability to edit and publish content within the project's wiki so long as the project is public and not inactive.

