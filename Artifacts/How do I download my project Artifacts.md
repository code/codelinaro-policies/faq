For public projects, all artifacts are available and do not require prior
authentication. Simply search for the project on our projects page and
select the Artifact icon to be taken to the resource. You'll then see a tree
of all downloadable artifacts. Your CodeLinaro account is authorized to use
the Artifact service for private or restricted artifacts. Please log into the
website and view your dashboard.

If you do not see the Artifact icon, please contact your administrator and
request service access. Otherwise, please continue by clicking the
**Artifact** icon. Once in the Artifact interface, make sure "Artifacts" is
selected. You will see all public artifacts as well as private/restricted ones
that you specifically are allowed to access.

Search for the artifact you need and you'll be able to download the resources.

