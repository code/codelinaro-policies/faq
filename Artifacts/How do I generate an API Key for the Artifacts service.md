Once you're in the Artifact service, you'll need to create an API Key for
yourself. Select your name on the top right-hand corner of the interface and
select the **Edit Profile** option.

In the **Authentication Settings** section, select **Generate API Key**.

After the key is generated, make a copy of the key and store it in a safe
place. This can be used to communicate with JFrog's Artifactory API or as a
"password" for HTML browsing of artifacts that are private or restricted.

