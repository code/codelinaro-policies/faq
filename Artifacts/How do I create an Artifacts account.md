Once you have obtained a CodeLinaro account, your organization's administrator
can allow you access to the CodeLinaro Artifacts Service. You will already be
able to access all public resources, but only be able to access private
project repositories as determined by your administrator. If you're missing
access to certain project resources, please contact your administrator.

