## Upload by Request

Upon request, CodeLinaro can automatically upload your artifact to a
project's artifact repository. To do so, you must request to [Upload an
Artifact](https://www.codelinaro.org/inner-support/upload-an-artifact/).

Fill in the request form:

  * Project

  * Source File Path - must be a publicly web-accessible URL

  * Destination File Path - Ex. path/to/file.txt

  * Checksum (optional)

  * If your file does not have any metadata to be associated, please set "Legacy" to "on/true".

  * Otherwise, fill in the rest of the fields as detailed as possible.

  * Click "Upload file" to submit the form

The Upload request will be sent to your administrator for approval. Once
approved, you'll be notified via email when the upload has been processed.

## Upload via Artifactory APIs

You can directly upload any artifact to your project's artifact repository
using the JFrog Artifactory APIs. You'll first need, permission, an [API
Token](https://www.codelinaro.org/faq/81756220/), and your client to make HTTP
requests to the APIs.

In order for you to push or upload artifacts to a project's repository via
the JFrog Artifactory APIs, you must be assigned the "Maintainer" role to your
project.

**NOTE** : Users with the role of "Developer" or "Reporter" cannot upload
artifacts using this method.

For further information on how this works, please see [JFrog's API
Documentation](https://www.jfrog.com/confluence/display/JFROG/Artifactory+REST+API#ArtifactoryRESTAPI-
DeployArtifact).

