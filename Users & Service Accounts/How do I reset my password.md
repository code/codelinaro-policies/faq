Please follow the steps below:

  1. Select the **Login** link on the right-hand side of the main navigation menu to bring up the login form.

  2. Select the " **Forgot Password?** " link.

  3. Enter your CodeLinaro username or primary email address.

  4. In a few moments, check your email and click the reset password link provided. You will only be able to access this link once. This link will expire within 30 minutes.

If you're still unable to reset your password, please "[Request
Support](https://www.codelinaro.org/support/)" and our CodeLinaro Support Team
will address accordingly.

