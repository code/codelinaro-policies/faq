To edit your account details, please start by logging into CodeLinaro and
selecting **Account Settings** from the user drop-down menu on the right-hand
side of the main navigation menu. Then, refer to the following:

  *  **Name** \- Select the **pencil icon** beside your name to bring up the edit account form. When you are finished editing your account information, click **OK**. The page will refresh and your display name changes will reflect across the other services within CodeLinaro in a couple minutes. If you cannot change your account details, please contact your administrator to modify them. Otherwise, please raise a support request, and our CodeLinaro Support Team will address it accordingly. <https://codelinaro.org/inner-support/modify-user/> (Login required)

  *  **Profile image** \- to configure your profile image, go to [_Gravatar_](https://en.gravatar.com/) and either create an account or associate an existing account with your email address.

