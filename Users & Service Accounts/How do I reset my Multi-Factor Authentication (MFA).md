## **Did you obtain a new device?**

CodeLinaro suggests you transfer your MFA app to your new device if you have a
new device. Depending on the MFA app, you can transfer credentials from one
device to another.

**Before you deactivate the old device** , you can reset the MFA yourself by
going to Account Settings and select on "Reset MFA" at the bottom of the page.

## **How to reset your MFA when you have a Recovery Code**

The Recovery Code can be used in place of the one-time code. Once Logged in,
you can then reset your MFA.

When prompted for your one-time code, select " _Try another method_."

![](https://www.codelinaro.org/images/mfa-reset-01.jpg)

Select " _Recovery Code_ " and enter your recovery code.

![](https://www.codelinaro.org/images/mfa-reset-02.jpg)

This will log you into CodeLinaro. You still need to reset your MFA by pulling
down on the CodeLinaro menu and going to " _Account Settings_." The bottom of
the page will allow you to Reset your MFA.

![](https://www.codelinaro.org/images/mfa-reset-03.jpg)

##  **Did you lose your device, or are you changing MFA apps?**

If you have a recovery code, you can reset your MFA; see the instructions
above.

If you do not have a recovery code, you will need to request an MFA reset by
requesting via email to CodeLinaro Support
([support@codelinaro.org](mailto:support@codelinaro.org)).

  * An MFA Reset requires a manual verification process to identify the user correctly, and we will fulfill your request as soon as you are validated. These requests require vetting the user's identity as outlined in the CodeLinaro Accounts Policy. 

  * CodeLinaro Support will email you at the address your account identified. You'll then be asked a few questions which must be answered to process this request. 

After verification, the following steps will be taken:

  * The Support Agent will set your MFA enrollment and notify you via email.

  * You can then login to CodeLinaro, where a new QR Code will prompt you to set up your MFA

 **It is very important to keep a copy of your one-time recovery code!**

