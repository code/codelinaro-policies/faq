This is a test article - DON'T DELETE YET!

When adding new content for the CLO FAQs, please use one or more of the
following tags/labels:

  * metrics

  * performance

  * repository

  * irc

  * support

  * known-issues

  * git

  * email

  * artifacts

  * wiki

  * projects-resources

  * getting-started

  * users-service-accounts

If you need a new tag, please let me know as the website/api will need to be
adjusted as well to fetch said tag/label.

