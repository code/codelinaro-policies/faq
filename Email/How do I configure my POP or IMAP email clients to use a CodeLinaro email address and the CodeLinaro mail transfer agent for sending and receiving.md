CodeLinaro uses Zoho Mail services. If you have an email client of choice
which supports either POP3 or IMAP connections, you'll be able to connect with
your CodeLinaro email account.

  * First, log into CodeLinaro and select the Mail icon from your dashboard. If you don't see this icon, contact your administrator about requesting a CodeLinaro email account.

  * If you've logged in successfully and selected the Mail icon, you should be brought to your email account at _mail.codelinaro.org_.

  * On the right side-bar navigation, select _Settings_ >> _Mail_ >> _Mail Accounts_ \- or click here: <https://mail.codelinaro.org/zm/#settings/mail/mailaccounts>

  * Selecting POP or IMAP will indicate the setting you'll need to then enter into your client. Information such as Server/Hostnames, Port and Mode will be needed by your client. 

For further information, please refer to the Zoho Mail documentation:
<https://www.zoho.com/mail/help/zoho-smtp.html>

