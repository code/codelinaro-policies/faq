If your organization needs to have multiple user accounts created in
CodeLinaro at one time or in bulk, our CodeLinaro Support agents are here to
help! The following process will create these accounts, but will also set up
the various service entitlements based on the information provided.

Please follow the steps below to proceed with this request:

  1. Download this [CSV template](https://www.codelinaro.org/users.csv) file.

  2. Enter all required information into the CSV file as it pertains to each user.

  3. Log into CodeLinaro and submit the "[Request to Create Multiple User Accounts](https://www.codelinaro.org/inner-support/create-user-bulk/)" form to create the request ticket needed.

  4. Proceed to the ticket you just created and upload (or drag-and-drop) your CSV file to this ticket for processing.

  5. Once the request is approved, a CodeLinaro Support Agent will begin processing the CSV creating these new accounts and will update you once the request is completed or if further clarification is needed.

