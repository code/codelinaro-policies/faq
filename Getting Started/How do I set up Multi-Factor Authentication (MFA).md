Upon receiving your "Welcome" email, you'll select the link to " _Set Your
Password "_. Once your password has been set, you'll be able to login to
CodeLinaro.

When you login for the first time or if you've had your MFA reset, you'll be
prompted to set up Multi-Factor Authentication (MFA).

**You 'll need to have an authenticator app. **

Below are a few options:

  * Google Authenticator 

  * Microsoft Authenticator (<https://www.microsoft.com/en-us/account/authenticator> ) 

  * Authy (<https://authy.com/download/> ) 

You'll need to scan the QR Code shown from the Secure Your Account screen.

![](https://www.codelinaro.org/fqa_setup_mfa1.png)

Once your app has scanned the QR Code, your app will provide you with a "one-
time code".

Enter the one-time code in the screen as shown and click **Continue.**

You will then be provided a "Recovery Code" to keep in case you need to access
CodeLinaro without your device or you need to reset your MFA. **It is very
important to keep a copy of your one-time recovery code!**

![](https://www.codelinaro.org/fqa_setup_mfa2.png)

Once you have copied this code, select the "I have safely recorded this code"
checkbox and select **Continue,** and you will be redirected to your
CodeLinaro Dashboard. **It is very important to keep a copy of your one-time
recovery code!**

