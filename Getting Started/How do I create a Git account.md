In order to gain non-public access to Git, you will first need a CodeLinaro
User Account. Once your account is in place, please make a request to your
organization's administrator to add the Git service to your account. Once
given access, you can log into your account on CodeLinaro, select "Git" from
your dashboard, and then navigate to which projects you specifically have
access.

