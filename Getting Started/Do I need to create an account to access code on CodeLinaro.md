All public projects hosted within CodeLinaro are open for any anonymous user
to access and download.

An account is required to access any private project and the full suite of
services provided by CodeLinaro.

Please speak with an organization's administrator, Technical Account Manager,
or Sales Account Manager to request an account within CodeLinaro. If you are
working with a vendor, please refer to their Release notes or documentation
for access and setup.

