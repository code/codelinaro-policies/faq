You can find the license list of a particular project you have access to in
the **Licenses** tab of the project page.

