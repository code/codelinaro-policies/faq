Once you have obtained a CodeLinaro account, your organization's administrator
can allow you access to the CodeLinaro Artifactory Service. You will already
be able to access all public resources but only access private project
repositories as determined by your administrator. Otherwise, please raise a
[support request](https://codelinaro.org/inner-support/enable-user-services/),
and our CodeLinaro Support Team will address it accordingly. If you're missing
access to specific project resources, please contact your administrator.

