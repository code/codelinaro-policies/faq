CodeLinaro partially supports GitHub services. Please raise a support request
if you would like to create/remove a repository or add/remove a user from a
repository, our CodeLinaro Support Team will address it accordingly.

  * [Create a repository (GitHub)](https://codelinaro.org/inner-support/create-a-repository-github/ )

Use this if you have a project not hosted in CodeLinaro Git.[
](https://codelinaro.org/inner-support/create-a-repository-github/ )

  * [Remove a repository (GitHub)](https://codelinaro.org/inner-support/remove-a-repository-github/)

Use this if you have a project removed from the CodeLinaro GitHub
organization.

  * [Add a user to repository(GitHub)](https://codelinaro.org/inner-support/add-a-user-to-repository-github/)

Use this to allow an existing GitHub user account access to said repository.

  * [Remove a user from repository(GitHub)]( https://codelinaro.org/inner-support/remove-a-user-from-repository-github/)

Use this to remove access for an existing GitHub user account from said
repository.

  

