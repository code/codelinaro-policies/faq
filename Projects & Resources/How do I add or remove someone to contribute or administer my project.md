Please contact your administrator. Your administrator will be able to assign
or unassign users with certain levels of access for specific projects.
Otherwise, please raise a support request, and our CodeLinaro Support Team
will address it accordingly.

  * [Request a user to be added to a project]( https://codelinaro.org/inner-support/add-users-to-project-role/ )

  * [Request a user to be removed from a project](https://www.codelinaro.org/inner-support/remove-users-to-project-role/)

