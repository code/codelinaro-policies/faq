You are trying to login to the CodeLinaro _Git_ but your account does not
access. In order to request access, login into
[_codelinaro.org_](https://www.codelinaro.org/) then click on _" About ->
Request Support"_

![](https://www.codelinaro.org/about-request-support.png)

At the support page, select the  " _Git " (left)  then "Enable service access"
(right)_

![](https://www.codelinaro.org/request-support-git-enable-service-access.png)

search your user, select the _Git service and click on "Update service
access"_

![](https://www.codelinaro.org/enable-service-git-access-user-update-service-access.png)

The system will automatically create a ticket that you can view.

![](https://www.codelinaro.org/enable-service-access-view-ticket.png)

At this point, you should have also received an email from
[_support@codelinaro.org_](mailto:support@codelinaro.org) which provided the
ticket status.

In case you do not have a Codelinaro account, please speak with an
organisation's administrator, Technical Account Manager, or Sales Account
Manager to request an account within CodeLinaro. If you are working with a
vendor, please refer to their Release notes or documentation for access and
setup.

