Security is an important aspect of CodeLinaro. Should you become aware of any
unauthorized use of, or access to, our Service through your Account, including
any unauthorized use of your password or Account, please contact us
immediately.

To contact Code Linaro, select the
[Contact](https://www.codelinaro.org/contact/) link at the bottom of any
CodeLinaro Page. On the Contact page, please fill in your name, email address
with the subject of **Report suspected breach of CodeLinaro or your account**.
Please provide any details or information on why you believe a breach has
occurred.

The CodeLinaro team will disable your account and contact you on the next
steps.

