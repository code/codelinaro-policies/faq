# Reset Password

To reset the password for your IRC Proxy account, you must send a [Service
Request](https://www.codelinaro.org/inner-support/service_request/) to
CodeLinaro. In the service request, please fill in the form and provide your
IRC user name in the description. CodeLinaro will provide you with a temporary
password. Please login and change it.

![](https://www.codelinaro.org/images/irc-pswd-reset-01.jpg)

# Change Password

To change your password, login to the IRC Proxy web interface
(https://irc.codelinaro.org:8443) and select "Your Settings". Change your
Password and select save.

![](https://www.codelinaro.org/images/irc-pswd-reset-02.jpg)
