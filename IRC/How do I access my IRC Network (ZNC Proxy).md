As a CodeLinaro user, you can request an account on the IRC Proxy service if
you're wanting use IRC without disconnecting, reconnecting and losing your
chat sessions. The service uses [ZNC](http://wiki.znc.in/ZNC), which is a
powerful application that has a lot of configuration options and modules that
alter the behavior and the features offered by the application.

## Request Access to IRC service

Before you can configure your IRC client to use the IRC Proxy service, you
**MUST  **raise a [CodeLinaro Support
Ticket](https://www.codelinaro.org/inner-support/request-user-access-to-irc/)
to ask for access to the IRC service. Access to the IRC service grants use of
the IRC Proxy service (ZNC) as well as the IRC Web Client (Lounge) service.

Please specify your IRC nickname. CodeLinaro Support will then add an account
to the service using your IRC nickname as the username.

A default password will be set in order to protect your account's settings
from unauthorized changes.

The ZNC modules [autoreply](http://wiki.znc.in/autoreply),
[chansaver](http://wiki.znc.in/chansaver), and [controlpanel
](http://wiki.znc.in/controlpanel)and will be enabled. You can change the
modules used but  **no support**  will be provided by CodeLinaro Support.

Support is limited to helping you get connected and troubleshooting any
**wide-spread**  problems with the server itself.

The _chansaver_ module ensures that any channels you join with your IRC client
will automatically be saved and remembered by ZNC.

This means that you don't need to configure your IRC client to automatically
join any channels; when your IRC client connects to ZNC, it will automatically
be re-connected to the channels that were in use previously.

Leaving a channel will cause that channel to be forgotten by ZNC, thus
maintaining the list.

## Configuring the IRC Client

If you don't already have an IRC client installed, here are some options you
can select and continue with this setup process:

  * [Pidgin](http://www.pidgin.im/)

  * [Konversation](http://konversation.kde.org/)

  * [xchat](http://xchat.org/)

  * [chatzilla](http://chatzilla.hacksrus.com/)

  * [irssi](http://www.irssi.org/)

Configure your IRC client to use irc.codelinaro.org on port 8443 with SSL.
Your IRC client must be configured with the server password in the format:
`username:password`

This is the  **ZNC account**  username and password.

CodeLinaro Support tries to keep it simple by setting your ZNC username to be
the same as your IRC nickname.

The password will be the long ZNC password provided to you by CodeLinaro
Support unless you have changed it.

When you disconnect from the IRC proxy, any  **direct**  messages sent to you
will be replied to by the server so that people know you are away.

All communications on the channels you were logged into are buffered until you
re-connect to the proxy, at which point they will be played back.

You can configure how big the buffer size is - the default is 50 lines.

## Configuring Your Account

ZNC provides a very flexible web interface for managing your account. Below is
a synopsis of how to access it and the sort of things you can do. For more
information, please see the [ZNC wiki](http://wiki.znc.in/ZNC).

After you have successfully completed your initial account configuration, some
settings are also available via messaging the virtual user *status. See
<https://wiki.znc.in/Using_commands>  for more.

To access the web interface, go to <https://irc.codelinaro.org:8443/>.

### Set your timezone.

When you reconnect to the IRC proxy server, buffered messages are played back
with a timestamp prefixing each line.

By default, the timestamp is in the GMT timezone. You will probably want to
change this to something more appropriate to you and

this can be done by scrolling down to the bottom of the main page where, under
**ZNC Behavior** , you will find the Timezone field.

Start to type your location and you should find the appropriate value to
select. Don't forget to click on Save!

### Change your password.

This is the password used to connect to ircproxy. It is  **NOT**  your
CodeLinaro Login password and it is not the password used to access any of the
channels.

### Change your IRC information.

These values probably don't need changing unless a mistake was made when
setting up the account, so change at your peril!

### Change network/channel information.

You can add channels if you want ZNC to buffer them for you. Also, you can (on
a per-channel basis) adjust how many lines of text you want it to keep for you
when disconnected.

None of the network-level modules are selected by default as what seems to be
the most important ones are enabled at the top-level. See the ZNC
documentation (linked to from each of the module names) for additional
information.

### Change top-level modules used.

As explained above, autoreply, chansaver and controlpanel are enabled by
default. The other modules have documentation on the ZNC web site if you want
to know more about what they do.

If you don't want to use ZNC's web interface, you can use commands via IRC:
<http://wiki.znc.in/Using_commands>

