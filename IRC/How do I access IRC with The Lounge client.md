# Request Access to IRC service

Before you can configure The Lounge client to use the service, you  **MUST
**raise a [CodeLinaro Support Ticket](https://www.codelinaro.org/inner-
support/request-user-access-to-irc/) to ask for access to the IRC service.
Access to the IRC service grants use of the IRC Proxy service (ZNC) as well as
the IRC Web Client (Lounge) service.

# Accessing The Lounge client

Go to <https://webirc.codelinaro.org/>. If you are prompted to log in, use
your normal CodeLinaro credentials. Please note that The Lounge uses single
sign-on, so you may not be prompted to log in.

# Configuring The Lounge client

The Lounge is a web-based IRC client. To start using it, you must configure
the client via the Lounge's [configuration
page](https://webirc.codelinaro.org/irc/#/connect). You should change the
**nick** to be the nick you have registered with the specified IRC network.

