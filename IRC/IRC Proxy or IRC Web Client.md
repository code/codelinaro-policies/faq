CodeLinaro provides two methods for caching IRC conversations when you are
disconnected: IRC Proxy and IRC Web Client.

The IRC Proxy is a service that requires you to have an IRC client application
on your own device that is configured to connect to the IRC Proxy service. The
proxy service caches any messages that occur when your IRC client application
is disconnected and plays them back when you reconnect.

The IRC Web Client is a web-based service that does not require any additional
software to be installed on your own device. You configure the web client with
the details of the IRC networks you want to connect to, and your IRC network
credentials. The web client caches any messages that occur when you do not
have a browser connected to the service.

Please note that you should only use **one** of these services. Since both
services remain connected to IRC on your behalf, it can cause clashes with IRC
nicks if both of them are used at the same time.

