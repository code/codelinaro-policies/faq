Please consider the following methods to maintain a reasonable repository sync
rate:

## Methods

  * Reduce sync frequency

  * Reduce sync size

    * Shallow method

    * Single branch method

  * Use clone bundle (Limited to Repo tool and clo/la project,  <https://git.codelinaro.org/clo/la> )

### Reduce sync frequency

  * Ensure that your command, script, or automation is not syncing repositories unnecessarily or at an unusually high frequency.

  * Do you perform a fresh sync of the same repository every time? If yes, consider caching and reusing the synced repositories. Please see the sections below for examples:

    * Use git fetch or git pull to update your local repository with changes from a remote repository

    * Updating to a new release using the Repo tool and previously synced repositories

  * Investigate whether multiple groups or processes within your organization are redundantly syncing the same repositories. If so, consider caching and sharing those repositories internally.

 _ **Use git fetch or git pull to update your local repository with changes
from a remote repository**_

If you have already synced a repository from CodeLinaro using the "git clone"
command and need to get the latest changes on the remote branch, you can use
either 'git fetch' or 'git pull' to retrieve the updates. For example,

 **Git fetch:**

`# Change directory to the local repository`

`$ cd [Path to the local repository]`

`# Specify remote name and branch name in your git fetch command`

`$ git fetch [remote-name] [branch-name]`

`# Once git fetch is complete, you can checkout the remote changes`

`$ git checkout [remote-name]/[branch-name]`

 **Git pull:**

`# Change directory to the local repository`

`$ cd [Path to the local repository]`

`# Specify remote name and branch name in your git pull command. The remote
changes will be pulled into your current branch. (You may need to resolve
merge conflict)`

`$ git pull [remote-name] [branch-name]`

 _ **Updating to a new release using the Repo tool and previously synced
repositories**_

If you have already synced repositories from CodeLinaro using the Repo tool
and our release manifest (For example,
AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913. **053**.xml) and need to sync a
new release (For example, AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.
**058**.00.xml), you can run "`repo init`" and "`repo sync`" commands in the
same workspace where you previously ran the sync. This will fetch only the new
changes and checkout to the new release.

 **Repo Tool (LA project example)**

`# Change directory to your workspace`

`$ cd [Path to your workspace]`

` `

`# Sync 053 release using the Repo tool`

`$ repo init --depth=1 \`

`-u `<https://git.codelinaro.org/clo/la/la/system/manifest.git>` \`

`-b release -m AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.053.00.xml \`

`--repo-url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo-branch=qc-stable`

`$ repo sync -j8`

` `

`# If you need to sync the new release, 058, you can run "repo init" and "repo
sync" in the same workspace.`

`$ repo init --depth=1 \`

`-u `<https://git.codelinaro.org/clo/la/la/system/manifest.git>` \`

`-b release -m AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.058.00.xml \`

`--repo-url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo-branch=qc-stable`

`$ repo sync -j8`

### Reduce sync size

Typically, users utilize the following tools to sync repositories:

  * Git

  * Repo tool

  * Sync_snap script

These tools offer both " **shallow** " and " **single branch** " methods. We
strongly recommend using one of these methods to minimize the size of your
sync, which offers the following benefits:

  * Faster sync time

  * Reduced storage required for sync

  * Reduced risk of sync failure due to network errors

####  Shallow method

The "shallow" method is highly recommended for users who only require the
latest revision or a single commit from the repository and does not need the
complete git history. This method results in the smallest possible sync size.
However, if you do require git history, we suggest using the "single branch"
method instead.

 **Git (Branch)**

`$ git clone --depth [depth] -b [branch] [remote-url]`

` # For example, this command clones only the latest revision of
LA.QSSI.12.0.c25 branch`

`# git clone --depth=1-b LA.QSSI.12.0.c25
`<https://git.codelinaro.org/clo/la/platform/art.git>` `

**Git (SHA Commit)**

`$ git fetch --depth [depth] [remote-name] [SHA1]`

`# For example, this command fetches only the 0a96461 commit`

`# git fetch --depth=1 origin 0a964610ebf1cf059c647182e53b860e8dfe756f`

 **Repo tool**

`$ repo init --depth=1 ...`

`$ repo sync ...`

`# For example, this command syncs only the revisions specified in the
AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.053.00.xml manifest file.`

`# repo init --depth=1 \`

`-u `<https://git.codelinaro.org/clo/la/la/system/manifest.git>` \`

`-b release -m AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.053.00.xml \`

`--repo-url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo-branch=qc-stable`

`# repo sync -j8`

 **Sync snap script**

`$ sync_snap_v2.sh --shallow_clone=true ...`

`# For example, this command syncs only the revisions specified in the
AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.097.00 xml manifest file.`

`# sync_snap_v2.sh --shallow_clone=true \`

`--image_type=la \`

`--tree_type=la_qssi \`

`--prop_opt=chipcode_hf \`

`--qssi_au_tag=AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.097.00 \`

`--workspace_path=<qssi_workspace_path> \`

`--common_oss_url=https://git.codelinaro.org \`

`--qssi_oss_manifest_git=clo/la/la/system/manifest \`

`--qssi_chipcode_hf_server=chipcode.qti.qualcomm.com \`

`--qssi_chipcode_hf_manifest_git=revision-
history/snapdragon_premium_high_2022-spf-2-0-la-
qssi-13-0-r1_src_history_manifests.git \`

`--qssi_chipcode_hf_manifest_branch=<customerID>-SRC_History \`

`--repo_url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo_branch=aosp-new/stable`

#### Single branch method

This "Single branch" method can be used to sync the git history of a specific
branch in the repository. The name of the branch can be specified either in
the command or in the manifest file, depending on the tool used. While the
sync size with this method may be larger than using the "shallow" method, it
is still smaller than syncing the entire repository, including all of its
commits and branches.

 **Git**

`$ git clone --branch [name] --single-branch [remote-url]`

`# For example, this command clones only the LA.QSSI.12.0.c25 branch`

`# git clone --branch LA.QSSI.12.0.c25 --single-branch
`<https://git.codelinaro.org/clo/la/platform/art.git>` `

**Repo tool**

`$ repo init --current-branch ...`

`$ repo sync ...`

`# For example, this command syncs only the branches specified in the
AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.053.00.xml manifest file.`

`# repo init --current-branch \`

`-u `<https://git.codelinaro.org/clo/la/la/system/manifest.git>` \`

`-b release -m AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.053.00.xml \`

`--repo-url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo-branch=qc-stable`

`# repo sync -j8`

 **Sync snap script**

`$ sync_snap_v2.sh --shallow_clone=false ...`

`# For example, this command syncs only the branches specified in the
AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.097.00 xml manifest file.`

`# sync_snap_v2.sh --shallow_clone=false \`

`--image_type=la \`

`--tree_type=la_qssi \`

`--prop_opt=chipcode_hf \`

`--qssi_au_tag=AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.097.00 \`

`--workspace_path=<qssi_workspace_path> \`

`--common_oss_url=https://git.codelinaro.org \`

`--qssi_oss_manifest_git=clo/la/la/system/manifest \`

`--qssi_chipcode_hf_server=chipcode.qti.qualcomm.com \`

`--qssi_chipcode_hf_manifest_git=revision-
history/snapdragon_premium_high_2022-spf-2-0-la-
qssi-13-0-r1_src_history_manifests.git \`

`--qssi_chipcode_hf_manifest_branch=<customerID>-SRC_History \`

`--repo_url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo_branch=aosp-new/stable`

### Use clone bundle (Limited to Repo tool and clo/la project,
<https://git.codelinaro.org/clo/la> )

Clone bundles are available for repositories in the clo/la project. We
highly recommend using these bundles when syncing any clo/la repository using
the Repo tool. The Repo tool utilizes clone bundle by default unless --no-
clone-bundle option is specified.

 **Repo tool**

`$ repo init --current-branch ...`

`$ repo sync ...`

`# For example, this command will utilize clone bundles in clo/la. Notice that
--no-clone-bundle option is not specified in the repo init command.`

`# repo init --current-branch \`

`-u `<https://git.codelinaro.org/clo/la/la/system/manifest.git>` \`

`-b release -m AU_LINUX_ANDROID_LA.QSSI.13.0.R1.11.00.00.913.053.00.xml \`

`--repo-url=https://git.codelinaro.org/clo/tools/repo.git \`

`--repo-branch=qc-stable`

`# repo sync -j8`

