Based on the configuration of CodeLinaro's Git service, git cloning and
fetching via HTTPS provides better performance for downloading objects.  All
traffic is routed over the Amazon Web Services (AWS) Global Accelerator (GA)
network.

 _" AWS Global Accelerator is a networking service that improves the
performance of your users' traffic by up to 60% using Amazon Web Services'
global network infrastructure. When the internet is congested, AWS Global
Accelerator optimizes the path to your application to keep packet loss,
jitter, and latency consistently low." \- AWS Global Accelerator._

Here are some of the reasons why we recommend using HTTPS over SSH:

  * Port 443, used by HTTPS, is open in any firewall that can access the internet. That isn't always the case for SSH. There are generally fewer firewall restrictions on HTTPS requests. 

  * HTTPS is the easiest to configure. If users need access to private repositories, an Access Token could be provided to perform pull or push requests for private repositories. All other public repositories don't require a Git account.

