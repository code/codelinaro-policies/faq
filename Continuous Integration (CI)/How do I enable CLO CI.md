At [codelinaro.org](http://codelinaro.org), go you your dashboard page and
select the project you want to enable it, then at then click on the '
**Enable CI '** bottom

![](https://www.codelinaro.org/project-dashboard-enable-ci.png)

At this point, you have a CLO project that can host public and private
repositories which may require CI in their process. The next step is to enable
CI at CLO Gitlab through ' **Settings -> General -> Visibility, project
features, permissions -> Repository -> enable CI/CD**' and ' **Save changes** '

![](https://www.codelinaro.org/gitlab-enable-ci.png)

CI is now enable but we still need to enable the _share_ _runners,_ where jobs
execute, go to ' **Settings -> CI/CD -> Runners -> Shared runners -> Enable
shared runners for this project**'

![](https://www.codelinaro.org/gitlab-enable-shared-runners.png)

Finally, create a `.gitlab-ci.yml` file at the project root folder and push
it to the main branch. At this point, Gitlab will automatically create a
_pipeline_ for every git event (or external event) and jobs will be executed
in one of the enabled runners. Pipeline would complete successfully in case
all stages complete without job failures.

