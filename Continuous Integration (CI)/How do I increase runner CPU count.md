To modify the default runner's CPU count, edit `.gitlab-ci.yml` and add the
following:

 _ **variables:**_

 _ **KUBERNETES_CPU_REQUEST: <how many CPUs, e.g. 2>**_

Each runner has a different limit, check [Runner Resource
Limits](https://www.codelinaro.org/faq/254443607/) for more info.

