There are at least 3 ways to use this pipeline depending on your project
needs:

  * "As-is" \- If no further adjustments are needed, you can simply reference this pipeline directly from your project using an include statement, or copy the `.gitlab-ci.yml` file over into your repository.

 _ **include:**_  
 _ **project: 'code/ci-library/reference-pipelines'**_  
 _ **file: 'kernel/make-kernel.yml'**_

  * "Add to Existing Pipeline" \- If you already have a pipeline, you can integrate this pipeline as a in your existing pipeline, by using an include statement and making sure your pipeline has a build stage defined.

 _ **stages:**_

 _ **\- build**_

 _ **-..**_

 _ **-reference-pipelines**_

 _ **include:**_  
 _ **project: 'code/ci-library/reference-pipelines'**_  
 _ **file: 'kernel/make-kernel.yml'**_

  * "Further configurations required" \- In this case, copy the corresponding reference pipeline file into your pipeline and feel free to make any adjustment.

CLO CI reference pipelines can be found at [CLO's git
repository](https://git.codelinaro.org/code/ci-library/reference-pipelines).

