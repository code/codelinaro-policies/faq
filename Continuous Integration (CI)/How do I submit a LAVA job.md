[LAVA](https://www.lavasoftware.org/index.html) is the main infrastructure and
environment used by Linaro to test previously produced artifacts on physical
and virtual devices becoming an essential piece of CI for many projects.

Below is a simple reference CI pipeline using the LAVA native tool `lavacli`

 **lava-qemu:**

 **before_script:**

 **\- apt update**

 **\- apt install -y lavacli**

 **script:**

 **\- JOB_ID=$(lavacli --uri**
**https://$LAVA_USERNAME:$LAVA_TOKEN@validation.linaro.org/RPC2/** **jobs
submit lava/lava-qemu-pipeline-first-job.yml)**

 **\- sleep 5**

 **\- echo "**[ **https://validation.linaro.org/scheduler/job/$JOB_ID
"**](https://validation.linaro.org/scheduler/job/$JOB_ID%22)

 **\- lavacli --uri**
**https://$LAVA_USERNAME:$LAVA_TOKEN@validation.linaro.org/RPC2/** **jobs logs
$JOB_ID | tee lava.log**

 **artifacts:**

 **paths:**

 **\- lava.log**

where the LAVA job definition was taken from [Submitting your first
job](https://master.lavasoftware.org/static/docs/v2/first-job.html)
documentation page.

