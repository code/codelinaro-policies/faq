Besides building your kernel using the standard tool `make` [Linux Kernel
Build](https://staging.codelinaro.org/faq/255393793/), you can use
[TuxBuild](https://docs.tuxsuite.com/#tuxbuild) to build a kernel image. In
this case, there is no need to install anything because there is already a
docker image prepared to run any `tuxsuite build` command. The only requisite
is to include the `TUXSUITE_TOKEN` as CI variable and adjust the below job for
your needs

 **build:**

 **image: tuxsuite/tuxsuite**

 **variables:**

 **GIT_REPO: '**[
**https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'**](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git')

 **GIT_REF: master**

 **TARGET_ARCH: arm64**

 **KCONFIG: defconfig**

 **TOOLCHAIN: gcc-9**

 **script:**

 **\- tuxsuite build --git-repo $GIT_REPO --git-ref $GIT_REF --target-arch
$TARGET_ARCH --kconfig $KCONFIG --toolchain $TOOLCHAIN**

At the job's log, you will find the link where you can see parameters,
results, timing and artifacts, e.g. [tuxsuite result
page](https://tuxapi.tuxsuite.com/v1/groups/codelinaro/projects/leonardo/builds/2TAuDjQ8QMkEP3bQFuwoT8gWKFQ).
If you do not have the `TUXSUITE_TOKEN`, get in contact with the [tuxsuite
team](https://docs.tuxsuite.com/).

One you have build your kernel, you can also test it with the same tool but
different parameter `tuxsuite test`. For example, below is a job example
testing a Kernel image `bzImage` using `qemu` `x86-64` emulation device

 **test:**

 **image: tuxsuite/tuxsuite**

 **variables:**

 **DEVICE: qemu-x86_64**

 **KERNEL:**[
**https://storage.tuxboot.com/x86_64/bzImage**](https://storage.tuxboot.com/x86_64/bzImage)

 **\- script:**

 **\- tuxsuite test --device $DEVICE --kernel $KERNEL --tests $TEST**

 **parallel:**

 **matrix:**

 **\- TEST: [ltp-smoke]**

You can also have a _tuxsuite plan_ which defines a set of kernel builds and
tests through a `yml` file, for more information read the [official
documentation](https://docs.tuxsuite.com/#tuxsuite-plan). Below is a sample
job that you can adjust to your needs, again just make sure you have the
`TUXSUITE_TOKEN` as CI variable

 **plan:**

 **image: tuxsuite/tuxsuite**

 **variables:**

 **GIT_REPO: '**[
**https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'**](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git')

 **GIT_REF: master**

 **before_script:**

 **\- |**

 **cat > plan.yaml << EOF**

 **version: 1**

 **name: kernel validation**

 **description: Build and test linux kernel for arm64 using all gcc
toolchains**

 **jobs:**

 **\- builds:**

 **\- {toolchain: gcc-8, target_arch: arm64, kconfig: defconfig}**

 **\- {toolchain: gcc-9, target_arch: arm64, kconfig: defconfig}**

 **\- {toolchain: gcc-10, target_arch: arm64, kconfig: defconfig}**

 **\- {toolchain: gcc-11, target_arch: arm64, kconfig: defconfig}**

 **\- {toolchain: gcc-12, target_arch: arm64, kconfig: defconfig}**

 **test: {device: qemu-arm64, tests: [ltp-smoke]}**

 **EOF**

 **script:**

 **\- tuxsuite plan --git-repo $GIT_REPO --git-ref $GIT_REF plan.yaml**

