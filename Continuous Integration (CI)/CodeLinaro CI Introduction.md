CodeLinaro CI is a service within CodeLinaro GitLab to allow automated tasks
to be carried out, typically builds and tests. Full documentation for the CI
service can be found at [GitLab CI/CD |
GitLab](https://docs.gitlab.com/ee/ci/)

CodeLinaro provides three _runners_ that are available to all projects to
execute CI workloads. Each runner has different characteristics and a tag (see
below for details) in a project's CI YAML file that can specify which of
these preconfigured runners is used.

The cost of executing a CI job is dependent on the resource specification, how
long the job takes to execute and how much data is transferred across the
network.

## Default Runner

If no runner tag is specified in the `.gitlab-ci.yml` file then the default
runner will be used.

  * Up to 4 CPUs available (defaults to 0.25 CPUs).

  * Up to 30GB memory available (defaults to 0.5GB).

  * 20GB ephemeral storage available.

  * Intel processors.

  * Note: It is **not** possible to use "Docker in Docker".

## Intel Runner

If the `amd64-runner` tag is specified in the `.gitlab-ci.yml` file then the
following characteristics will apply:

  * Up to 60 CPUs available (defaults to 2 CPUs).

  * Up to 256GB memory available (defaults to 4GB).

  * Up to 2TB ephemeral storage available (defaults to 20GB).

  * Intel processors.

  * Possible to request a GPU.

  * Note: It **is** possible to use "Docker in Docker".

## Arm Runner

If the `arm64-runner` or `aarch64-runner` tag is specified in the `.gitlab-
ci.yml` file, then the following characteristics will apply:

  * Up to 60 CPUs available (defaults to 2 CPUs).

  * Up to 256GB memory available (defaults to 4GB).

  * Up to 2TB ephemeral storage available (defaults to 20GB).

  * Arm processors.

  * Note: It **is** possible to use "Docker in Docker."

## Specifying Which Runner

To specify the runner tag, edit `.gitlab-ci.yml` and add the following:

 _ **default:**_  
 _ **tags:**_  
 _ **- <runner tag>**_

where `<runner tag>` must be `amd64-runner`, `arm64-runner` or
`aarch64-runner`. If you want to use the default runner, do **not** specify a
runner tag.

## Specifying Resource Requirements

As noted above, you can specify how many CPUs, how much memory, and how much
ephemeral storage is required. To specify one or more of these resource
requirements, edit `.gitlab-ci.yml` and add the following:

 _ **variables:**_  
 _ **KUBERNETES_CPU_REQUEST: <how many CPUs, e.g. 2>**_  
 _ **KUBERNETES_MEMORY_REQUEST: <how much memory, e.g. 8G>**_  
 _ **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: <how much storage, e.g. 32G>**_

Each of these lines is optional and, if omitted, the default for the specified
runner will be applied.

## Requesting a GPU

To request compute with a GPU attached, add the following declaration to the
`.gitlab-ci.yml` file:

 _ **variables:**_  
 _ **KUBERNETES_POD_ANNOTATIONS_1: "GPU=NVIDIA"**_

Note that this service is currently only supported when using the
`amd64-runner`.

## Persistent storage

Not to be confused with GitLab CI Cache or GitLab CI Artifact storage.
Persistent storage is available per git repository and is charged back to the
project for that repository. There is an hourly charge for persistent
storage, based on how much data is stored in it. Once set up, the storage
remains in existence until you turn it off. Turning off persistent storage
results in that storage being deleted and all data lost. At the moment, It is
not possible to share persistent storage between git repositories, but efforts
are underway to support this.

### Turning on persistent storage

If your a project admin or system admin, from your CodeLinaro dashboard:

  * Click on the project name

  * Click on the Edit button

  * Click on the Git tab

  * Right-click on the appropriate git repository and click "Turn On Persistent Storage"

You will be asked to confirm that you want to turn it on. * _Please note that
it can take several minutes for persistent storage to be set up._

### Using persistent storage

After enabling persistent storage for a git repository, include the following
declaration within the .gitlab-ci.yml file:

 _ **variables:**_

 _ **      KUBERNETES_POD_ANNOTATIONS_1: "MOUNT_EFS=/data"**_

Note:

  * If using this feature as well as enabling GPU, the ANNOTATIONS line should say "_2" not "_1".

  * This feature can be used on all runners.

  * The path after "MOUNT_EFS" is where the persistent storage will be mounted.

