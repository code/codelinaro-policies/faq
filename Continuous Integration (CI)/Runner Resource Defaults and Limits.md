Table below shows Runner Resource defaults and limits. If you specify a value
out of these ranges, CI infrastructure would not be able to pick a runner and
the job would fail. Numbers in **bold** indicate the default value for each
resource. The tag

**Runner**

|

 **Architecture**

|

Tag

|

 **CPU**

|

 **Memory**

|

 **Ephemeral Storage**  
  
---|---|---|---|---|---  
  
Default

|

Intel

|

none or `default-runner`

|

 **0.25** -4

|

 **0.5GB** -30GB

|

 **20GB**  
  
ARM

|

Arm

|

`arm64-runner`

|

 **2** -60

|

 **4GB** -256GB

|

 **20GB** -2TB  
  
Intel

|

Intel

|

`amd64-runner`

|

 **2** -60

|

 **4GB** -256GB

|

 **20GB** -256GB

