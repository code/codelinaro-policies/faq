To modify the default runner's ephemeral storage, edit `.gitlab-ci.yml` and
add the following:

 _ **variables:**_

 _ **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: <how much storage, e.g. 32G>**_

Each runner has a different limit, check [Runner Resource
Limits](https://www.codelinaro.org/faq/254443607/) for more info.

