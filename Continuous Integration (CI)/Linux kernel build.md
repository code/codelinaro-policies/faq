Building the kernel has become an easy task but under the CLO CI there are a
details to care. Below is a pipeline configuration which can be used as a
reference to compile ARM binaries under both runner architectures. After
installing the required packages, the kernel master branch is fetched and
compiled under both runner architectures; for the Intel runner, two toolchains
are used to generate ARM binaries (cross-compilation) and for the ARM, the
native toolchain (native compilation). High CPU count and memory is not
strictly required, so values can be lowered, just keep in mind that compiling
the kernel is a pretty CPU demanding so lowering the CPU count would increase
the build time. Finally, note that several artifacts are set for later
consumption, e.g. LAVA

 **.kernel-base:**

 **variables:**

 **KERNEL_TAR_URL:**[
**https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git/+archive/refs/heads/master.tar.gz**](https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git/+archive/refs/heads/master.tar.gz)

 **KUBERNETES_CPU_REQUEST: 60**

 **KUBERNETES_MEMORY_REQUEST: 64G**

 **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 20G**

 **image: debian:bullseye-slim**

 **.debian-package-dependencies: &debian-package-dependencies**

 **\- apt update**

 **\- apt install -y wget build-essential libncurses-dev bison flex libssl-dev
libelf-dev bc ${GCC}**

 **.build-kernel: &build-kernel**

 **\- wget $KERNEL_TAR_URL**

 **\- tar xf master.tar.gz**

 **\- make defconfig**

 **\- make all -j$(nproc)**

 **kernel-amd64:**

 **extends: .kernel-base**

 **before_script:**

 **\- *debian-package-dependencies**

 **script:**

 **\- *build-kernel**

 **parallel:**

 **matrix:**

 **\- ARCH: arm64**

 **GCC: gcc-aarch64-linux-gnu**

 **CROSS_COMPILE: aarch64-linux-gnu-**

 **\- ARCH: arm**

 **GCC: gcc-arm-linux-gnueabihf**

 **CROSS_COMPILE: arm-linux-gnueabihf-**

 **tags:**

 **\- amd64-runner**

 **artifacts:**

 **paths:**

 **\- vmlinux**

 **\- System.map**

 **\- arch/$ARCH/boot/Image**

 **kernel-arm64:**

 **extends: .kernel-base**

 **before_script:**

 **\- *debian-package-dependencies**

 **script:**

 **\- *build-kernel**

 **tags:**

 **\- arm64-runner**

 **artifacts:**

 **paths:**

 **\- vmlinux**

 **\- System.map**

 **\- arch/arm64/boot/Image**

