To modify the default runner's memory, edit `.gitlab-ci.yml` and add the
following:

 _ **variables:**_

 _ **KUBERNETES_MEMORY_REQUEST: <how much memory, e.g. 8G>**_

Each runner has a different limit, check [Runner Resource
Limits](https://www.codelinaro.org/faq/254443607/) for more info.

