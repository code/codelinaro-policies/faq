The CLO CI, which is based on Gitlab, uses `.gitlab-ci.yml` file for the CI/CD
configuration. In this file we define the structure and the order of jobs that
runners will execute.

First we start with list of stages for jobs, that also defines their order of
execution:

 _ **stages:**_

 _ **\- build**_

 _ **\- test**_

 _ **\- deploy**_

As a next step we define the jobs for each stage:

 _ **build-job:**_

 _ **stage: build**_

 _ **script:**_

 _ **\- echo "compiling the code" && sleep 1**_

 _ **\- echo "compiling completed"**_

 _ **test-job-1:**_

 _ **stage: test**_

 _ **script:**_

 _ **\- echo "Running 1st set of tests" && sleep 5**_

 _ **\- echo "1st set of tests completed"**_

 _ **test-job-2:**_

 _ **stage: test**_

 _ **script:**_

 _ **\- echo "Running 2nd set of tests" && sleep 5**_

 _ **\- echo "2nd set of tests completed"**_

 _ **deploy-job:**_

 _ **stage: deploy**_

 _ **script:**_

 _ **\- echo "Pushing binaries to FTP" && sleep 3**_

 _ **\- echo "Pushing binaries to FTP completed"**_

After committing `.gitlab-ci.yml` the pipeline should be picked by a runner
and the progress can be observed in CI/CD tab of the repository. If it is not
there, check if pipeline syntax is correct and CI/CD is enabled. Pipelines can
be triggered manually from UI without the need for a commit event. Also,
pipelines can be triggered programmatically, go to 'Settings -> CI/CD ->
Pipeline triggers' at the git repository for more information.

