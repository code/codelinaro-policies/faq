Besides submitting your LAVA jobs directly using the native LAVA tool
`lavacli` as indicated in [How do I submit a LAVA
job?](https://staging.codelinaro.org/faq/259031059/) you can submit jobs
through [SQUAD](https://squad.readthedocs.io/en/latest/) which stands for
_Software Quality Dashboard,_ offering an easy way to monitor multiple LAVA
jobs that are submitted in a single CI pipeline.

For example, you have the following scenario: a CI pipeline that produces
multiple artifacts and you want to test them with LAVA; from here you have two
options, submitting directly into LAVA or through SQUAD, both ultimately doing
the same task but SQUAD offers you a single dashboard for all your submitted
jobs providing a better insight of your tests.

Below is a sample job, submitting two jobs (in this case both are the same but
in a real scenario would be different of course) through SQUAD. It is assumed
that you have already created the corresponding SQUAD project and set the CI
variable `SQUAD_TOKEN`, the latter obtained from the user settings at the
SQUAD instance, e.g. <https://qa-reports.linaro.org/_/settings/api-token/>

**variables:**

 **LAVA_SERVER:**<http://validation.linaro.org>

 **LAVA_JOB_DEFINITION: lava/lava-qemu-pipeline-first-job.yml**

 **SQUAD_SERVER:**<https://qa-reports.linaro.org> ****

**SQUAD_GROUP: ~leonardo.sandoval**

 **SQUAD_PROJECT: codelinaro**

 **squad-qemu:**

 **variables:**

 **SQUAD_VERSION: $CI_PIPELINE_ID**

 **before_script:**

 **\- apt update**

 **\- apt install -y curl jq**

 **script:**

 **\- |**

 **for job in $(seq 2); do**

 **TESTJOB_ID= "$(curl --header "Auth-Token: $SQUAD_TOKEN" \**

 **\--form backend=$LAVA_SERVER \**

 **\--form definition=@$LAVA_JOB_DEFINITION \**

**$SQUAD_SERVER/api/submitjob/$SQUAD_GROUP/$SQUAD_PROJECT/$SQUAD_VERSION/gitlab-
ci-env) " curl $SQUAD_SERVER/api/testjobs/$TESTJOB_ID/ | jq .**

 **done**

 **\- echo $SQUAD_SERVER/$SQUAD_GROUP/$SQUAD_PROJECT/build/$SQUAD_VERSION/**

