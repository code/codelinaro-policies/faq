Besides building the kernel as shown in the [Kernel builds with TuxSuite build
and plan](https://staging.codelinaro.org/faq/258277428/) FAQ, _TuxSuite_ can
also be used to generate a complete Linux distribution with the Yocto
Project & Open Embedded environment using the `tuxsuite bake` command. For
more information, check the [official
documentation](https://docs.tuxsuite.com/#tuxsuite-bake). Below is a sample
job that can be used as reference for further work

 **bake:**

 **image: tuxsuite/tuxsuite**

 **before_script:**

 **\- |**

 **cat > build-definition.json << EOF**

 **{**

 **" container": "ubuntu-20.04",**

 ** "distro": "oniro-linux",**

 **" environment": {**

 **" TEMPLATECONF": "../oniro/flavours/linux"**

 **},**

 **" envsetup": "oe-core/oe-init-build-env",**

 **" machine": "qemux86-64",**

 **" sources": {**

 **" repo": {**

 **" branch": "kirkstone",**

 **" default.xml",**

 **" url": "**[ **https://gitlab.eclipse.org/eclipse/oniro-core/oniro
"**](https://gitlab.eclipse.org/eclipse/oniro-core/oniro%22)

 **}**

 **},**

 **" target": "intltool-native"**

 **}**

 **EOF**

 **script:**

 **\- tuxsuite bake submit build-definition.json**

