Linaro uses OpenEmbedded and Yocto projects to generate Linux distributions
for embedded systems. Due to the relevance for the company, below is a
reference pipeline using persistent storage for download (`DL_DIR`) and share-
state (`SSTATE_DIR`) directories, this way pipelines will share previously
build artifacts thus build times will reduce drastically (no need to
recompile).

Note that we pick `kirkstone` as reference stable branch but any other stable
or mainline branch (`master`) would work fine.

 **.mount-variables:**

 **variables:**

 **MOUNT_POINT: "/data"**

 **KUBERNETES_POD_ANNOTATIONS_1: "MOUNT_EFS=${MOUNT_POINT}"**

 **DL_DIR: ${MOUNT_POINT}/kirkstone/DL_DIR**

 **SSTATE_DIR: ${MOUNT_POINT}/kirkstone/SSTATE_DIR**

 **#Workaround: this pre-job is required to prepare the mount point:**

 **#the yocto image crops/yocto:ubuntu-20.04-base has**

 **#yoctouser as default users which has no permission**

 **#on the mount point, so this job sets the right permissions**

 **poky-prepare-mount:**

 **stage: .pre**

 **extends: .mount-variables**

 **variables:**

 **DL_DIR: ${MOUNT_POINT}/kirkstone/DL_DIR**

 **SSTATE_DIR: ${MOUNT_POINT}/kirkstone/SSTATE_DIR**

 **script:**

 **\- set -ex**

 **\- mkdir -p $DL_DIR $SSTATE_DIR**

 **\- chmod -R 777 $DL_DIR $SSTATE_DIR**

 **\- ls -la $DL_DIR $SSTATE_DIR**

 **poky:**

 **image: crops/yocto:ubuntu-20.04-base**

 **extends: .mount-variables**

 **variables:**

 **KUBERNETES_CPU_REQUEST: 60**

 **KUBERNETES_MEMORY_REQUEST: 64G**

 **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 100G**

 **script:**

 **\- git clone**<https://git.yoctoproject.org/poky> ****

**\- cd poky**

 **\- git checkout -t origin/kirkstone**

 **\- source oe-init-build-env**

 **\- echo "DL_DIR ?= \"$DL_DIR\"" >> conf/local.conf**

 **\- echo "SSTATE_DIR ?= \"$SSTATE_DIR\"" >> conf/local.conf**

 **\- bitbake core-image-sato**

 **parallel:**

 **matrix:**

 **\- MACHINE: [qemuarm64, qemux86-64, qemuarm]**

 **tags:**

 **\- amd64-runner**

