To request compute with a GPU attached, add the following declaration to the
`.gitlab-ci.yml` file:

 _ **variables:**_

 _ **KUBERNETES_POD_ANNOTATIONS_1: "GPU=NVIDIA"**_

Note that this service is currently only supported when using the
`amd64-runner`. Below is a pipeline configuration where GPU is attached and
the cmd `nvidia-smi` is used to query GPU device state

 **gpu:**

 **variables:**

 **KUBERNETES_POD_ANNOTATIONS_1: "GPU=NVIDIA"**

 **script:**

 **\- apt update**

 **\- DEBIAN_FRONTEND=noninteractive apt-get install -y mesa-utils**

 **\- nvidia-smi**

 **parallel:**

 **matrix:**

 **\- TAG: [amd64-runner]**

 **tags:**

 **\- $TAG**

