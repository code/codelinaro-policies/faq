Not to be confused with GitLab CI Cache or GitLab CI Artifact storage.
Persistent storage is available per git repository and is charged back to the
project for that repository. There is an hourly charge for persistent
storage, based on how much data is stored in it. Once set up, the storage
remains in existence until you turn it off. Note that turning off persistent
storage results in that **storage being deleted and all data lost** so make
sure do not really care losing this data before taking this action. At the
moment, It is not possible to share persistent storage between git
repositories, but efforts are underway to support this.

### Turning on persistent storage

If your a project admin or system admin, from your CodeLinaro dashboard:

  * Click on the project name

  * Click on the Edit button

  * Click on the Git tab

  * Right-click on the appropriate git repository and click "Turn On Persistent Storage"

You will be asked to confirm that you want to turn it on. * _Please note that
it can take several minutes for persistent storage to be set up._

### Using persistent storage

After enabling persistent storage for a git repository, include the following
declaration within the `.gitlab-ci.yml` file:

 _ **variables:**_

 _ **KUBERNETES_POD_ANNOTATIONS_1: "MOUNT_EFS=/data"**_

Note:

  * If using this feature as well as enabling GPU, the ANNOTATIONS line should say `_2` not `_1`.

  * This feature can be used on all runners.

  * The path after `MOUNT_EFS` is where the persistent storage will be mounted.

