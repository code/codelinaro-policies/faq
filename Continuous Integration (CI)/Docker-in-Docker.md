All runners except the default one, are able to run docker commands on CI
jobs. Below is a job example using the `docker:dind` image and launching
`dockerd` daemon at the `before_script` phase, then running docker commands.
Note that jobs required either `arm64-runner` or `amd64-runner` tags,
otherwise CI will pick the default runner (`fargate`) which is not able to run
docker-in-docker.

 **dind:**

 **image: docker:dind**

 **before_script:**

 **\- dockerd -l fatal &**

 **\- sleep 5**

 **script: |**

 **set -ex**

 **docker info**

 **docker login -u $DOCKER_USER -p $DOCKER_PASSWORD**

 **mkdir -p dockerfile**

 **cat > dockerfile/Dockerfile << EOF**

 **FROM python:3.8-slim**

 **RUN apt-get update**

 **EOF**

 **tag=lsandov1/clo-ci-recipes-dind:${CI_COMMIT_SHORT_SHA}**

 **latest=lsandov1/clo-ci-recipes-dind:latest**

 **docker build --pull --tag ${tag} --tag ${latest} dockerfile/**

 **docker run ${tag} python --version**

 **docker run ${latest} python --version**

 **docker push ${tag}**

 **docker push ${latest}**

 **tags:**

 **\- arm64-runner**

