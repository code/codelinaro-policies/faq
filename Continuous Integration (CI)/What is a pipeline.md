Pipelines are the top-level component of continuous integration and
delivery/deployment.

There are made of:

  * Jobs, which define what to do - for example compile the code

  * Stages, which define when to run the jobs - for example stages that run tests after stages that compile the code

Pipelines and their jobs and stages are defined in a pipeline configuration
file for each project. By default, the `.gitlab-ci.yml` file is used.

## Behavior of a classical pipeline

Jobs and as a result stages and pipelines are executed by job runners.
Multiple jobs of the same stage can be executed in parallel - for example
compiling the code for different targets, but it is important to remember that
each job is executed in a isolated environment.

If any job in any stage fails, the next stages are not executed and the
pipeline ends early.

Typical pipeline may consist of three stages executed in the following order:

  * build

  * test

  * deploy

Pipelines and jobs can be configured with multiple dependencies and triggers
to archive the desired result. To create a simple pipeline at CLO CI, check
[How to create a pipeline?](https://staging.codelinaro.org/faq/251363363/)

