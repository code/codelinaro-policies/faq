There are several resources you can increase on runners including number of
_CPUs_ , _memory_ and _ephemeral storage_. To specify one or more of these
resource requirements, edit `.gitlab-ci.yml` and add the following:

 _ **variables:**_

 _ **KUBERNETES_CPU_REQUEST: <how many CPUs, e.g. 2>**_

 _ **KUBERNETES_MEMORY_REQUEST: <how much memory, e.g. 8G>**_

 _ **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: <how much storage, e.g. 32G>**_

Note that variables can be global or local. If all jobs would have the same
amount of resources, set the variables in a global scope (outside any job)

 **variables:**

 **KUBERNETES_CPU_REQUEST: 2**

 **KUBERNETES_MEMORY_REQUEST: 8G**

 **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 32G**

 **demo1:**

 **script:**

 **\- echo 'HW resources taken from global scope' demo2:**

 **script:**

 **\- echo 'HW resources taken from global scope'**

otherwise set the required variables on each particular job

 **demo1:**

 **variables:**

 **KUBERNETES_CPU_REQUEST: 2**

 **KUBERNETES_MEMORY_REQUEST: 8G**

 **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 32G**

 **script:**

 **\- echo 'HW resources taken from job scope'**

 **demo2:**

 **variables:**

 **KUBERNETES_CPU_REQUEST: 4**

 **KUBERNETES_MEMORY_REQUEST: 16G**

 **KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 4G**

 **script:**

 **\- echo 'HW resources taken from job scope'**

